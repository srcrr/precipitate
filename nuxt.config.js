export default {
  srcDir: "./src",
  css: [
    '@vuikit/theme/dist/vuikit.min.css',
  ],
  env: {
    MYVAR: process.env.MYVAR,
    MAPBOX_TOKEN: process.env.MAPBOX_TOKEN,
    OPEN_WEATHER_MAP_API_KEY: process.env.OPEN_WEATHER_MAP_API_KEY,
  },
  serverMiddleware: [
    /**
     * Middleware can't be compiled directly, so we'll need to
     * use tsc to compile it.
     */
    { path: "/api", handler: "~/api/js/index.js" },
  ],
  buildModules: ['@nuxt/typescript-build'],
  components: true,
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.stylus$/,
          use: [
            'stylus-loader'
          ]
        },
        {
          test: /\.pug$/,
          use: [
            'pug-loader'
          ]
        }
      ]
    }
  }
}
