# Precipitate - Weather Forecast to Your Calendar

This project gets the weather forecast for a location and
translates it into an ics format. This allows you to import it
into a calendar application.

## Prerequisites

- Node v12.18.3 (npm v6.14.6)
- yarn v 1.22.4

### Environment Variables

The system relies on external services: MapBox and OpenWeatherMap. An API key is required for both of these. Once you obtain the API key set the following environment variables accordingly:

- `MAPBOX_TOKEN` to your mapbox token
- `OPEN_WEATHER_MAP_API_KEY` to your OpenWeatherMap API key.

They can also be set in the `.env` file within the source directory.
Additionally, the app runtime can be affected by the `NODE_ENV`
variable. Typicall the values `development` and `production` are used.

## Installation

```bash
yarn install
```

## Testing

```bash
yarn test
```

## Start the Server

```bash
yarn dev
```

The server should be up at `http://localhost:3000`

## Making Changes to the API

The API is written in TypeScript, and as there isn't a nodemon watching
the directory yet the code needs to be compile manually.

To do so, enter the `api` directory and type:

```bash
npm
```