import express from "express";
import HttpStatus from "http-status-codes";
import {Precipitator} from "./precipitator";

const OWM_API_KEY = process.env.OPEN_WEATHER_MAP_API_KEY;

const app = express();

app.use('/:units/:lat/:lon/weather.ics', function (req : any, resp : any, next : any) {

    if (!OWM_API_KEY) {
        resp.status(HttpStatus.INTERNAL_SERVER_ERROR).send("An internal server error occurred.");
        return;
    }
    Precipitator.handle(req, resp, next, OWM_API_KEY);

});


export default app;