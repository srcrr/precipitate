import chai from "chai";
import { UnitType, Precipitator, OwmParams, ForecastEntry, Forecast, HIGH_SYMBOL, LOW_SYMBOL } from './precipitator';
import chaiHttp from 'chai-http';
import chaiAsPromised from "chai-as-promised";
import HttpStatus from "http-status-codes";
import debug from 'debug';
const log = debug('server');
import 'mocha';
import e from "express";
import { EventAttributes, ReturnObject } from "ics";
import app from "./app";

chai.use(chaiHttp);
chai.should();
chai.use(chaiAsPromised);
const expect = chai.expect;
const assert = chai.assert;

const TEST_LAT = 33.9872
const TEST_LON = -118.3214

const OWM_API_KEY = process.env.OPEN_WEATHER_MAP_API_KEY;

function checkOwmKey() {
    if (!OWM_API_KEY) {
        throw new Error("Please set the OPEN_WEATHER_MAP_API_KEY environment variable")
    }
}

function getTestPrecipitator(unitType = UnitType.imperial) {
    return new Precipitator(unitType, TEST_LON, TEST_LAT, OWM_API_KEY || "");
}

describe('Precipitator Tests', () => {
    it('Can correctly create an instance', () => {
        checkOwmKey();
        let p = new Precipitator(UnitType.imperial, TEST_LAT, TEST_LON, OWM_API_KEY || "");
        let expextedUrl = `/f/${TEST_LAT}/${TEST_LON}/weather.ics`;
        chai.assert.equal(p.serviceUrl, expextedUrl);
        let expectedParams = { lat: TEST_LON, lon: TEST_LAT, units: "imperial", appid: OWM_API_KEY, exclude: "hourly,minutely,current" } as OwmParams;
        chai.assert.deepEqual(p.owmParams, expectedParams);
    }),
        it('Weather can be retrieved successfully', (done) => {
            checkOwmKey();
            let precip = getTestPrecipitator()
            // precip.fetchForecast().should.eventually.have.property("daily");
            precip.fetchForecast().then(function (forecast) {
                expect(forecast.daily.length).to.equal(8);
                done();
            }, function (err) {
                done(err);
            })
        })
    it("Calendar (imperial values) can be rendered successfully", (done) => {
        checkOwmKey();
        let precip = getTestPrecipitator()
        precip.fetchCalendar().then(function (calendar) {
            assert.isNotNull(calendar);
            done();
        }, function (err) {
            done(err);
        })
    })
    it("Calendar (metric values) can be rendered successfully", (done) => {
        checkOwmKey();

        let precip = getTestPrecipitator(UnitType.metric);
        precip.fetchCalendar().then(function (calendar) {
            assert.isNotNull(calendar);
            done();
        }, function (err) {
            done(err);
        });
    }),
        it("Requests can be written successfully", (done) => {
            let precip = getTestPrecipitator(UnitType.imperial);
            let url = precip.serviceUrl;
            chai.assert.isNotNull(url);
            if (!url) return;
            chai.request(app).get(url)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res).to.have.status(HttpStatus.OK);
                    expect(res).to.have.header("Content-Type", "text/calendar; charset=utf-8");
                    expect(res.text).to.contain(HIGH_SYMBOL);
                    expect(res.text).to.contain(LOW_SYMBOL);
                    expect(res.text).to.contain("℉");
                    done();
                })
        }),
        it("An error is raised with an invalid latitude", (done) => {
            let url = `/f/sillyboy/${TEST_LON}/weather.ics`;
            chai.assert.isNotNull(url);
            if (!url) return;
            chai.request(app).get(url)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(err).to.be.null;
                    expect(res).to.have.status(HttpStatus.NOT_ACCEPTABLE);
                    done();
                })
        }),
        it("Only 1 forecast is shown per day", (done) => {
            let precip = getTestPrecipitator(UnitType.imperial);
            precip.fetchForecast().then((fc) => {
                let events = precip.renderEvents(fc);
                assert.lengthOf(events, 8);
                done();
            }).catch((err) => {
                done(err);
            })
        }),
        it("An error is raised with an invalid longitude", (done) => {
            let url = `/f/${TEST_LAT}/sillybody/weather.ics`;
            chai.assert.isNotNull(url);
            if (!url) return;
            chai.request(app).get(url)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res).to.have.status(HttpStatus.NOT_ACCEPTABLE);
                    done();
                })
        }),
        it("An error is raised with an invalid metric", (done) => {
            let url = `/g/${TEST_LAT}/${TEST_LON}/weather.ics`;
            chai.assert.isNotNull(url);
            if (!url) return;
            chai.request(app).get(url)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res).to.have.status(HttpStatus.NOT_ACCEPTABLE);
                    done();
                })
        })
});
