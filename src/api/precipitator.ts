import { Router, RequestHandler, Response, Request, NextFunction } from "express";
import axios from "axios";
import { EventAttributes, ReturnObject, createEvent, createEvents } from "ics";
import debug from 'debug';
import moment from "moment";
import HttpStatus from "http-status-codes";
const log = debug('server');

const TEXT_CALENDAR = "text/calendar; charset=utf-8";

export const enum UnitType {
    imperial = "f",
    metric = "c"
}

export const HIGH_SYMBOL = `\u{2197}`;
export const LOW_SYMBOL = `\u{2198}`;

export const IconMap = {
    "Clear": "🌣",
    // "Clear": "🌤",
    "Clouds": "🌥",
    "Drizzle": "🌦",
    "Rain": "🌧",
    "Snow": "🌨",
    "Thunderstorm": "🌩",
    "Mist": "🌫",
    "Smoke": "🌫",
    "Haze": "🌫",
    "Fog": "🌫",
    "Sand": "🌫",
} as any

export type OwmParams = {
    units: string | null,
    lat: number | null,
    lon: number | null,
    exclude: string,
    appid: string,
}

export type ForecastEntry = {
    dt: number,
    sunrise: number,
    sunset: number,
    temp: {
        day: number,
        min: number,
        max: number,
        night: number,
        eve: number,
    },
    weather: {
        id: number,
        main: string,
        description: string,
        icon: string,
    } []
}

/**
 * In later versions we may add more of the fields.
 * For now just get the bare necessities.
 */
export type Forecast = {
    lat: number,
    lon: number,
    daily: ForecastEntry[],
}

export type WeatherEvent = {
    title: string,
    start: number[],
    duration: {
        days: number,
    }
}

export class Precipitator {

    public OWM_URL = "https://api.openweathermap.org/data/2.5/onecall";

    constructor(
        public units: UnitType | null,
        public lat: number | null,
        public lon: number | null,
        public owmKey: string,
    ) { }

    get serviceUrl(): string | null {
        if (!(this.units && this.lat && this.lon))
            return null;
        return `/${this.units}/${this.lat}/${this.lon}/weather.ics`;
    }

    get owmParams(): null | OwmParams {
        if (!(this.units && this.lat && this.lon))
            return null;
        let units = "metric";
        if (this.units == UnitType.imperial)
            units = "imperial";
        return {
            units: units,
            lat: this.lon,
            lon: this.lat,
            exclude: "hourly,minutely,current",
            appid: this.owmKey,
        };
    }

    public async fetchForecast(): Promise<Forecast> {
        let resp = await axios.get(this.OWM_URL, { params: this.owmParams });
        let forecast = <Forecast>resp.data;
        return forecast;
    }

    private getDateList(m: moment.Moment, dayOffset: number = 0): number[] {
        m = m.add(dayOffset, "days");
        m = m.add(1, "months");
        return [m.year(), m.month(), m.date()]
    }

    public get tempUnits(): string {
        return this.units == UnitType.metric ? "℃" : "℉";
    }

    public renderEvents(forecast: Forecast): EventAttributes[] {
        return forecast.daily.map((day) => {
            let dt = moment.unix(day.dt);
            let desc = day.weather[0].main;
            if (day.weather[0].description)
                desc = desc + " (" + day.weather[0].description + ")"
            let icon = "";
            if (day.weather[0].main in IconMap) {
                icon = IconMap[day.weather[0].main] + " ";
            }
            let title = `${icon}${HIGH_SYMBOL}${day.temp.max}${this.tempUnits} ${LOW_SYMBOL}${day.temp.min}${this.tempUnits} - ${desc}`;
            let start = this.getDateList(dt);
            let end = this.getDateList(dt, 1);
            debug(`Creating event`);
            debug(`${JSON.stringify({title, start, end})}`);
            return { title, start, duration: {days: 1} } as EventAttributes;
        });
    }

    public async fetchCalendar(): Promise<ReturnObject> {
        let forecast = await this.fetchForecast();
        let events = this.renderEvents(forecast);
        let cal = createEvents(events);
        if (cal.error) {
            // console.error(cal.error);
            cal.error.message += `\nCurrent values: ${JSON.stringify(events)}`;
            throw cal.error;
        }
        return cal;
    }

    public async writeCalendar(resp: Response): Promise<any> {
        let cal = await this.fetchCalendar();
        resp.setHeader("Content-Type", TEXT_CALENDAR);
        resp.setHeader('Content-Disposition', 'attachment; filename="weather.ics"')
        return resp.send(cal.value);
    }

    static async handle(req : Request, res : Response, next : NextFunction, owmKey : string) {
        let u = req.params["units"];
        let lat = req.params["lat"] as any;
        let lon = req.params["lon"] as any;
        if (isNaN(lat)) {
            console.log(`lat is not a number ${lat}`);
            res.status(HttpStatus.NOT_ACCEPTABLE).send(`Longitude must me a number (got ${lat})`);
            return false;
        }
        if (isNaN(lon)) {
            console.log(`lon is not a number ${lon}`);
            res.status(HttpStatus.NOT_ACCEPTABLE).send(`Longitude must me a number (got ${lon})`);
            return false;
        }
        if (!["c", "f"].includes(u)) {
            console.log("invalid unit");
            res.status(HttpStatus.NOT_ACCEPTABLE).send(`Invalid unit; expected "c" or "f," got ${u}`);
            return false;
        }
        console.log("passed error checks");
        let units = (u == "c") ? UnitType.metric : UnitType.imperial;
        let p = new Precipitator(units, lat, lon, owmKey);
        try {
            await p.writeCalendar(res);
            next();
            return true;
        } catch(err) {
            next(err);
            return false;
        }
    }

    /**
     * Update fields for url construction
     * @param units
     * @param lat 
     * @param lon 
     */
    public update(units: UnitType | null, lat: number | null, lon: number | null) {
        this.units = units;
        this.lat = lat;
        this.lon = lon;
        return this;
    }
}