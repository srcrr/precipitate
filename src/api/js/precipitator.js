"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Precipitator = exports.IconMap = exports.LOW_SYMBOL = exports.HIGH_SYMBOL = void 0;
const axios_1 = __importDefault(require("axios"));
const ics_1 = require("ics");
const debug_1 = __importDefault(require("debug"));
const moment_1 = __importDefault(require("moment"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const log = debug_1.default('server');
const TEXT_CALENDAR = "text/calendar; charset=utf-8";
exports.HIGH_SYMBOL = `\u{2197}`;
exports.LOW_SYMBOL = `\u{2198}`;
exports.IconMap = {
    "Clear": "🌣",
    // "Clear": "🌤",
    "Clouds": "🌥",
    "Drizzle": "🌦",
    "Rain": "🌧",
    "Snow": "🌨",
    "Thunderstorm": "🌩",
    "Mist": "🌫",
    "Smoke": "🌫",
    "Haze": "🌫",
    "Fog": "🌫",
    "Sand": "🌫",
};
class Precipitator {
    constructor(units, lat, lon, owmKey) {
        this.units = units;
        this.lat = lat;
        this.lon = lon;
        this.owmKey = owmKey;
        this.OWM_URL = "https://api.openweathermap.org/data/2.5/onecall";
    }
    get serviceUrl() {
        if (!(this.units && this.lat && this.lon))
            return null;
        return `/${this.units}/${this.lat}/${this.lon}/weather.ics`;
    }
    get owmParams() {
        if (!(this.units && this.lat && this.lon))
            return null;
        let units = "metric";
        if (this.units == "f" /* imperial */)
            units = "imperial";
        return {
            units: units,
            lat: this.lon,
            lon: this.lat,
            exclude: "hourly,minutely,current",
            appid: this.owmKey,
        };
    }
    async fetchForecast() {
        let resp = await axios_1.default.get(this.OWM_URL, { params: this.owmParams });
        let forecast = resp.data;
        return forecast;
    }
    getDateList(m, dayOffset = 0) {
        m = m.add(dayOffset, "days");
        m = m.add(1, "months");
        return [m.year(), m.month(), m.date()];
    }
    get tempUnits() {
        return this.units == "c" /* metric */ ? "℃" : "℉";
    }
    renderEvents(forecast) {
        return forecast.daily.map((day) => {
            let dt = moment_1.default.unix(day.dt);
            let desc = day.weather[0].main;
            if (day.weather[0].description)
                desc = desc + " (" + day.weather[0].description + ")";
            let icon = "";
            if (day.weather[0].main in exports.IconMap) {
                icon = exports.IconMap[day.weather[0].main] + " ";
            }
            let title = `${icon}${exports.HIGH_SYMBOL}${day.temp.max}${this.tempUnits} ${exports.LOW_SYMBOL}${day.temp.min}${this.tempUnits} - ${desc}`;
            let start = this.getDateList(dt);
            let end = this.getDateList(dt, 1);
            debug_1.default(`Creating event`);
            debug_1.default(`${JSON.stringify({ title, start, end })}`);
            return { title, start, duration: { days: 1 } };
        });
    }
    async fetchCalendar() {
        let forecast = await this.fetchForecast();
        let events = this.renderEvents(forecast);
        let cal = ics_1.createEvents(events);
        if (cal.error) {
            // console.error(cal.error);
            cal.error.message += `\nCurrent values: ${JSON.stringify(events)}`;
            throw cal.error;
        }
        return cal;
    }
    async writeCalendar(resp) {
        let cal = await this.fetchCalendar();
        resp.setHeader("Content-Type", TEXT_CALENDAR);
        resp.setHeader('Content-Disposition', 'attachment; filename="weather.ics"');
        return resp.send(cal.value);
    }
    static async handle(req, res, next, owmKey) {
        let u = req.params["units"];
        let lat = req.params["lat"];
        let lon = req.params["lon"];
        if (isNaN(lat)) {
            console.log(`lat is not a number ${lat}`);
            res.status(http_status_codes_1.default.NOT_ACCEPTABLE).send(`Longitude must me a number (got ${lat})`);
            return false;
        }
        if (isNaN(lon)) {
            console.log(`lon is not a number ${lon}`);
            res.status(http_status_codes_1.default.NOT_ACCEPTABLE).send(`Longitude must me a number (got ${lon})`);
            return false;
        }
        if (!["c", "f"].includes(u)) {
            console.log("invalid unit");
            res.status(http_status_codes_1.default.NOT_ACCEPTABLE).send(`Invalid unit; expected "c" or "f," got ${u}`);
            return false;
        }
        console.log("passed error checks");
        let units = (u == "c") ? "c" /* metric */ : "f" /* imperial */;
        let p = new Precipitator(units, lat, lon, owmKey);
        try {
            await p.writeCalendar(res);
            next();
            return true;
        }
        catch (err) {
            next(err);
            return false;
        }
    }
    /**
     * Update fields for url construction
     * @param units
     * @param lat
     * @param lon
     */
    update(units, lat, lon) {
        this.units = units;
        this.lat = lat;
        this.lon = lon;
        return this;
    }
}
exports.Precipitator = Precipitator;
