"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const precipitator_1 = require("./precipitator");
const OWM_API_KEY = process.env.OPEN_WEATHER_MAP_API_KEY;
const app = express_1.default();
app.use('/:units/:lat/:lon/weather.ics', function (req, resp, next) {
    if (!OWM_API_KEY) {
        resp.status(http_status_codes_1.default.INTERNAL_SERVER_ERROR).send("An internal server error occurred.");
        return;
    }
    precipitator_1.Precipitator.handle(req, resp, next, OWM_API_KEY);
});
exports.default = app;
