"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importDefault(require("chai"));
const precipitator_1 = require("./precipitator");
const chai_http_1 = __importDefault(require("chai-http"));
const chai_as_promised_1 = __importDefault(require("chai-as-promised"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const debug_1 = __importDefault(require("debug"));
const log = debug_1.default('server');
require("mocha");
const app_1 = __importDefault(require("./app"));
chai_1.default.use(chai_http_1.default);
chai_1.default.should();
chai_1.default.use(chai_as_promised_1.default);
const expect = chai_1.default.expect;
const assert = chai_1.default.assert;
const TEST_LAT = 33.9872;
const TEST_LON = -118.3214;
const OWM_API_KEY = process.env.OPEN_WEATHER_MAP_API_KEY;
function checkOwmKey() {
    if (!OWM_API_KEY) {
        throw new Error("Please set the OPEN_WEATHER_MAP_API_KEY environment variable");
    }
}
function getTestPrecipitator(unitType = "f" /* imperial */) {
    return new precipitator_1.Precipitator(unitType, TEST_LON, TEST_LAT, OWM_API_KEY || "");
}
describe('Precipitator Tests', () => {
    it('Can correctly create an instance', () => {
        checkOwmKey();
        let p = new precipitator_1.Precipitator("f" /* imperial */, TEST_LAT, TEST_LON, OWM_API_KEY || "");
        let expextedUrl = `/f/${TEST_LAT}/${TEST_LON}/weather.ics`;
        chai_1.default.assert.equal(p.serviceUrl, expextedUrl);
        let expectedParams = { lat: TEST_LON, lon: TEST_LAT, units: "imperial", appid: OWM_API_KEY, exclude: "hourly,minutely,current" };
        chai_1.default.assert.deepEqual(p.owmParams, expectedParams);
    }),
        it('Weather can be retrieved successfully', (done) => {
            checkOwmKey();
            let precip = getTestPrecipitator();
            // precip.fetchForecast().should.eventually.have.property("daily");
            precip.fetchForecast().then(function (forecast) {
                expect(forecast.daily.length).to.equal(8);
                done();
            }, function (err) {
                done(err);
            });
        });
    it("Calendar (imperial values) can be rendered successfully", (done) => {
        checkOwmKey();
        let precip = getTestPrecipitator();
        precip.fetchCalendar().then(function (calendar) {
            assert.isNotNull(calendar);
            done();
        }, function (err) {
            done(err);
        });
    });
    it("Calendar (metric values) can be rendered successfully", (done) => {
        checkOwmKey();
        let precip = getTestPrecipitator("c" /* metric */);
        precip.fetchCalendar().then(function (calendar) {
            assert.isNotNull(calendar);
            done();
        }, function (err) {
            done(err);
        });
    }),
        it("Requests can be written successfully", (done) => {
            let precip = getTestPrecipitator("f" /* imperial */);
            let url = precip.serviceUrl;
            chai_1.default.assert.isNotNull(url);
            if (!url)
                return;
            chai_1.default.request(app_1.default).get(url)
                .end(function (err, res) {
                if (err)
                    return done(err);
                expect(res).to.have.status(http_status_codes_1.default.OK);
                expect(res).to.have.header("Content-Type", "text/calendar; charset=utf-8");
                expect(res.text).to.contain(precipitator_1.HIGH_SYMBOL);
                expect(res.text).to.contain(precipitator_1.LOW_SYMBOL);
                expect(res.text).to.contain("℉");
                done();
            });
        }),
        it("An error is raised with an invalid latitude", (done) => {
            let url = `/f/sillyboy/${TEST_LON}/weather.ics`;
            chai_1.default.assert.isNotNull(url);
            if (!url)
                return;
            chai_1.default.request(app_1.default).get(url)
                .end(function (err, res) {
                if (err)
                    return done(err);
                expect(err).to.be.null;
                expect(res).to.have.status(http_status_codes_1.default.NOT_ACCEPTABLE);
                done();
            });
        }),
        it("Only 1 forecast is shown per day", (done) => {
            let precip = getTestPrecipitator("f" /* imperial */);
            precip.fetchForecast().then((fc) => {
                let events = precip.renderEvents(fc);
                assert.lengthOf(events, 8);
            }).catch((err) => {
                done(err);
            });
        }),
        it("An error is raised with an invalid longitude", (done) => {
            let url = `/f/${TEST_LAT}/sillybody/weather.ics`;
            chai_1.default.assert.isNotNull(url);
            if (!url)
                return;
            chai_1.default.request(app_1.default).get(url)
                .end(function (err, res) {
                if (err)
                    return done(err);
                expect(res).to.have.status(http_status_codes_1.default.NOT_ACCEPTABLE);
                done();
            });
        }),
        it("An error is raised with an invalid metric", (done) => {
            let url = `/g/${TEST_LAT}/${TEST_LON}/weather.ics`;
            chai_1.default.assert.isNotNull(url);
            if (!url)
                return;
            chai_1.default.request(app_1.default).get(url)
                .end(function (err, res) {
                if (err)
                    return done(err);
                expect(res).to.have.status(http_status_codes_1.default.NOT_ACCEPTABLE);
                done();
            });
        });
});
